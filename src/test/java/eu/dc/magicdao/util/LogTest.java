package eu.dc.magicdao.util;

import eu.dc.magicdao.util.enums.LogLevel;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by dc on 15.01.2016.
 */
public class LogTest {

    @Test
    public void logTest() {
        Log.setLogLevel(LogLevel.EMERGENCY);
        assertEquals(Log.write("test", LogLevel.EMERGENCY), true);
        assertEquals(Log.write("test", LogLevel.DEBUG), false);
    }
}
