package eu.dc.magicdao.mongodb;

import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import eu.dc.magicdao.DAO;
import eu.dc.magicdao.mongodb.entities.Beitrag;
import eu.dc.magicdao.mongodb.entities.Profil;
import eu.dc.magicdao.mongodb.entities.Veranstaltungen;
import eu.dc.magicdao.operator.defaults.mongodb.MongoDB;
import org.bson.Document;
import org.junit.Ignore;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

/**
 * Created by dc on 22.04.2016.
 */
@Ignore
public class MongoDBDaoSystemTest {

    private static final String DATABASE_NAME = "test";

    @Test
    public void shouldCreateNewObjectInEmbeddedMongoDb() {
        MongoClient mongoClient = new MongoClient();
        MongoDatabase db = mongoClient.getDatabase("connectmepls");
        DAO.init(new MongoDB(db));
        assertEquals(DAO.insert(new Profil("W", "D", "C", null)), true);
    }

    @Test
    public void updatethis() {
        MongoClient mongoClient = new MongoClient();
        MongoDatabase db = mongoClient.getDatabase("connectmepls");
        DAO.init(new MongoDB(db));
        assertTrue((Boolean) DAO.update(new Profil("Willi", "A", "B", "FUGHGGJSGSDSJFC")));
    }

    @Test
    public void loadthis() {
        MongoClient mongoClient = new MongoClient();
        MongoDatabase db = mongoClient.getDatabase("connectmepls");
        DAO.init(new MongoDB(db));
        Profil p = (Profil) DAO.load(new Profil("FUGHGGJSGSDSJFC"));
        assertEquals(p.gibE(), "Willi");
    }

    @Test
    public void findthemall() {
        MongoClient mongoClient = new MongoClient();
        MongoDatabase db = mongoClient.getDatabase("connectmepls");
        DAO.init(new MongoDB(db));
        List<Object> all = DAO.findAll(Profil.class);
        System.out.println(all.size());
    }

    @Test
    public void integrationTest() {
        /* Beispiele
        1  Alle User holen
        2. Alle Emails holen
        3. Bestimmten User suchen
        4. Bestimmte E-Mail suchen
        5. User erzeugen
        6. User löschen
        7. Beitrag erstellen
        8. Alle veranstaltungen in einer bestimmten Stadt
        9. Durchschnittsbewertung einer Veranstaltung
        */

        List<Object> all = DAO.findAll(Profil.class);
        System.out.println("1) Get all profiles: (" + all.size() + ")");
        System.out.println("2) Get all emails:");
        Profil foundprofilbyid = null;
        Profil foundprofilbyemail = null;
        for (Object o : all) {
            Profil p = (Profil) o;
            System.out.print(p.gibE() + ", ");
            if (p.email.equalsIgnoreCase("mail@orf.at"))
                foundprofilbyemail = p;
            if (p._ID.contains("56ec09490f6e26ce4cf1e7a1"))
                foundprofilbyid = p;
        }
        System.out.println("");
        System.out.println("3) Searching user with ID: (56ec09490f6e26ce4cf1e7a1)");
        System.out.println("Email: " + foundprofilbyid.email + " ID: " + foundprofilbyid._ID);
        System.out.println("");
        System.out.println("4) Searching user with email: (@orf.at)");
        System.out.println("Email: " + foundprofilbyemail.email + " ID: " + foundprofilbyemail._ID);
        System.out.println("");
        System.out.println("5) Creating User Test");
        DAO.insert(new Profil("Test", "Test", "Test", null));
        System.out.println("");
        System.out.println("6) Delete User"); //TODO: Delete Mark
        Profil x = new Profil("Mark", "Mark", "Mark", "Mark" + new Random().nextInt(10000));
        DAO.insert(x);
        System.out.println("");
        System.out.println("7) Creating Beitrag");
        DAO.insert(new Beitrag("Test", "Test", null));
        System.out.println("");
        System.out.println("8) Alle Veranstaltungen in der Stadt");
        List<Object> veranstaltungen = DAO.findAll(Veranstaltungen.class);
        Veranstaltungen p = null;
        for (Object o : veranstaltungen) {
            p = (Veranstaltungen) o;
            System.out.print(p + ", ");
            if (p.ort.equalsIgnoreCase(""))//TODO: Ort eintragen
                System.out.print(p + ", ");

        }
        System.out.println("");
        System.out.println("9) Durchschnittsbewertung einer Veranstaltung");
        BasicDBObject w = new BasicDBObject();

        ArrayList<Object> w2t = p.grades;
        double b = 0;

        for (Object obj : p.grades) {
            b += ((Document) obj).getDouble("grade");
            System.out.println(obj);
        }

        b /= p.grades.size();
        System.out.println(b);
    }
}
