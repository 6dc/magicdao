package eu.dc.magicdao.mongodb.entities;

import eu.dc.magicdao.annotations.Column;
import eu.dc.magicdao.annotations.Table;

import java.util.ArrayList;

/**
 * Created by Patrick on 22.04.2016.
 */
@Table(tablename = "veranstaltung")
public class Veranstaltungen {
    @Column(fieldname = "name")
    public String name;
    @Column(fieldname = "bild")
    public String bild;
    @Column(fieldname = "beschreibung")
    public String beschreibung;
    @Column(fieldname = "beginn")
    public String beginn;
    @Column(fieldname = "bewertung")
    public ArrayList grades;
    @Column(fieldname = "ort")
    public String ort;
    @Column(fieldname = "_id", primarykey = true)
    public String _id;
}
