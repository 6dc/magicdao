package eu.dc.magicdao.mongodb.entities;

import eu.dc.magicdao.annotations.Column;
import eu.dc.magicdao.annotations.Table;

/**
 * Created by Patrick on 22.04.2016.
 */
@Table(tablename = "beitrag")
public class Beitrag {
    @Column(fieldname = "text")
    private String text;
    @Column(fieldname = "datum")
    private String datum;
    @Column(fieldname = "_id", primarykey = true)
    private String _id;

    public Beitrag(String text, String datum, String _id) {
        this.text = text;
        this.datum = datum;
        this._id = _id;
    }
}
