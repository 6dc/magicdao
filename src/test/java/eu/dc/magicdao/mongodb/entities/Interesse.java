package eu.dc.magicdao.mongodb.entities;

import eu.dc.magicdao.annotations.Column;
import eu.dc.magicdao.annotations.Table;

/**
 * Created by Patrick on 22.04.2016.
 */
@Table(tablename = "interesse")
public class Interesse {
    @Column(fieldname = "_id", primarykey = true)
    private String _id;
    @Column(fieldname = "bezeichnung")
    private String bezeichnung;
}
