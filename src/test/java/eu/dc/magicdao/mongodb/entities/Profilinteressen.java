package eu.dc.magicdao.mongodb.entities;

import eu.dc.magicdao.annotations.Column;
import eu.dc.magicdao.annotations.Table;

/**
 * Created by Patrick on 22.04.2016.
 */
@Table(tablename = "profilinteressen")
public class Profilinteressen {
    @Column(fieldname = "_id", primarykey = true)
    private String _id;
    @Column(fieldname = "privatprofil_id")
    private String privatprofil_id;
    @Column(fieldname = "interessen_id")
    private String interessen_id;
    @Column(fieldname = "kommentar")
    private String kommentar;
}
