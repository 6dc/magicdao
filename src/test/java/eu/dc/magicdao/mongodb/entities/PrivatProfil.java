package eu.dc.magicdao.mongodb.entities;

import eu.dc.magicdao.annotations.Column;
import eu.dc.magicdao.annotations.Table;

import java.util.Date;

/**
 * Created by Patrick on 22.04.2016.
 */
@Table(tablename = "privatprofil")
public class PrivatProfil {
    @Column(fieldname = "vorname")
    private String vorname;
    @Column(fieldname = "nachname")
    private String nachname;
    @Column(fieldname = "geburtsdatum")
    private Date geburtsdatum;
    @Column(fieldname = "übermich")
    private String übermich;
    @Column(fieldname = "profil_id")
    private String profil_id;
}
