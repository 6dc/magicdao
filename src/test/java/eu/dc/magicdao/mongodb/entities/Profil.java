package eu.dc.magicdao.mongodb.entities;

import eu.dc.magicdao.annotations.Column;
import eu.dc.magicdao.annotations.Table;

/**
 * Created by Patrick on 22.04.2016.
 */
@Table(tablename = "profil")
public class Profil {
    @Column(fieldname = "e_mail")
    public String email;
    @Column(fieldname = "passwort")
    public String passwort;
    @Column(fieldname = "profilbild")
    public String profilbild;
    @Column(fieldname = "_id", primarykey = true)
    public String _ID;

    public Profil(String email, String passwort, String profilbild, String _ID) {
        this.email = email;
        this.passwort = passwort;
        this.profilbild = profilbild;
        this._ID = _ID;
    }

    public Profil(String _ID) {
        this._ID = _ID;
    }

    public Profil() {
    }

    public String gibE() {
        return email;
    }
}
