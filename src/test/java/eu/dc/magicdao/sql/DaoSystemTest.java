package eu.dc.magicdao.sql;

import eu.dc.magicdao.DAO;
import eu.dc.magicdao.exception.MagicDaoException;
import eu.dc.magicdao.operator.defaults.SQL.SQLDAO;
import eu.dc.magicdao.sql.entities.User;
import eu.dc.magicdao.util.Log;
import eu.dc.magicdao.util.enums.LogLevel;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;

import static java.sql.DriverManager.getConnection;
import static org.junit.Assert.assertEquals;

/**
 * Created by dc on 07.12.2015.
 */
public class DaoSystemTest {
    public static boolean first = true;
    User user;

    @Before
    public void initObjects() throws SQLException {
        user = new User("Max Musterman", "klartext Passwort", new Date());
        if (!first) {
            return;
        }
        first = false;
        Log.setLogLevel(LogLevel.DEBUG);

        Connection connection = getConnection("jdbc:hsqldb:mem:quiz", "SA", "");
        connection.createStatement().execute("DROP SCHEMA PUBLIC CASCADE");
        connection.createStatement().execute("CREATE TABLE i_interests( i_name VARCHAR(250), i_prio INT , primary key(i_name))");
        connection.createStatement().execute("INSERT INTO i_interests(i_name, i_prio) VALUES ('Tennis',1)");
        connection.createStatement().execute("CREATE TABLE u_users( u_id INT , u_username VARCHAR(250), u_password VARCHAR(250) ,u_i_interests VARCHAR(250), u_favday DATETIME , primary key(u_id),FOREIGN KEY (u_i_interests) REFERENCES  i_interests(i_name))");
        connection.createStatement().execute("INSERT INTO u_users( u_id  , u_username, u_password ,u_i_interests, u_favday) VALUES (2, 'Manfred', 'klartext Passwort' ,'Tennis', NOW())");
        connection.createStatement().execute("INSERT INTO u_users( u_id  , u_username, u_password ,u_i_interests, u_favday) VALUES (1, 'Manfred', 'klartext Passwort','Tennis' , NOW())");
        connection.createStatement().execute("INSERT INTO u_users( u_id  , u_username, u_password ,u_i_interests, u_favday) VALUES (3, 'Manfred', 'klartext Passwort','Tennis' , NOW())");

        DAO.init(new SQLDAO(connection));


    }

    @Test
    public void testInsertObject() {
        assertEquals(DAO.insert(user), true);
        user.setId(Integer.MIN_VALUE);
        assertEquals(DAO.insert(user), true);
    }

    @Test
    public void testUpdateObject() {

        user.setId(1);
        user.username = "Manfred";
        assertEquals(DAO.update(user), true);
    }

    @Test
    public void testLoadObject() throws NoSuchFieldException {
        User user = new User("", "", null);
        user.setId(2);
        assertEquals(((User) DAO.load(user)).username, "Manfred");
    }


    @Test
    public void testFindObject() throws NoSuchFieldException {
        User user = new User("Manfred", "", null);
        //assertEquals(((User) DAO.findby(user, user.getClass().getField("username"))).password, "klartext Passwort");
    }

    @Test(expected= MagicDaoException.class)
    public void testDeleteObject() throws NoSuchFieldException {
        user.setId(138);
        assertEquals(DAO.insert(user), true);
        DAO.delete(user);

        User u = (User) DAO.load(user);
    }

    @Test
    public void testFindAll() {
    }
}
