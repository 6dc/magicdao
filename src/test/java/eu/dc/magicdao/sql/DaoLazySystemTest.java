package eu.dc.magicdao.sql;

import eu.dc.magicdao.DAO;
import eu.dc.magicdao.sql.entities.LazyUser;
import eu.dc.magicdao.operator.defaults.SQL.SQLDAO;
import eu.dc.magicdao.util.Log;
import eu.dc.magicdao.util.enums.LogLevel;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;

import static java.sql.DriverManager.getConnection;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Created by dc on 07.12.2015.
 */
public class DaoLazySystemTest {
    public static boolean first = true;
    LazyUser user;

    @Before
    public void initObjects() throws SQLException {
        user = new LazyUser("Max Musterman", "klartext Passwort", new Date());
        if (first) {
            first = false;
            Log.setLogLevel(LogLevel.DEBUG);
            Connection connection = getConnection("jdbc:hsqldb:mem:quiz", "SA", "");
            connection.createStatement().execute("DROP SCHEMA PUBLIC CASCADE");
            connection.createStatement().execute("CREATE TABLE i_interests( i_name VARCHAR(250), i_prio INT , primary key(i_name))");
            connection.createStatement().execute("INSERT INTO i_interests(i_name, i_prio) VALUES ('Tennis',1)");
            connection.createStatement().execute("CREATE TABLE u_users( u_id INT , u_username VARCHAR(250), u_password VARCHAR(250) ,u_i_interests VARCHAR(250), u_favday DATETIME, u_f_id INT, primary key(u_id),FOREIGN KEY (u_i_interests) REFERENCES  i_interests(i_name))");
            connection.createStatement().execute("CREATE TABLE f_flaschen( f_id INT, f_name VARCHAR(250), f_u_id INT, primary key(f_id),FOREIGN KEY (f_u_id) REFERENCES  u_users(u_id))");
            connection.createStatement().execute("INSERT INTO u_users( u_id  , u_username, u_password ,u_i_interests, u_favday) VALUES (2, 'Manfred', 'klartext Passwort' ,'Tennis', NOW())");
            connection.createStatement().execute("INSERT INTO u_users( u_id  , u_username, u_password ,u_i_interests, u_favday) VALUES (1, 'Manfred', 'klartext Passwort','Tennis' , NOW())");
            connection.createStatement().execute("INSERT INTO u_users( u_id  , u_username, u_password ,u_i_interests, u_favday) VALUES (3, 'Manfred', 'klartext Passwort','Tennis' , NOW())");
            connection.createStatement().execute("INSERT INTO f_flaschen(f_id, f_name,f_u_id) VALUES (1,'PerodieFlasche',1)");
            connection.createStatement().execute("INSERT INTO f_flaschen(f_id, f_name,f_u_id) VALUES (2,'Nightlife',1)");
            connection.createStatement().execute("INSERT INTO f_flaschen(f_id, f_name,f_u_id) VALUES (3,'Daylife',2)");
            connection.createStatement().execute("INSERT INTO f_flaschen(f_id, f_name,f_u_id) VALUES (4,'Dao',2)");
            connection.createStatement().execute("INSERT INTO f_flaschen(f_id, f_name,f_u_id) VALUES (5,'DieFlasche',3)");
            connection.createStatement().execute("INSERT INTO f_flaschen(f_id, f_name,f_u_id) VALUES (6,'JovanDieFlasche',3)");

            DAO.init(new SQLDAO(connection));
        }

    }

    @Test
    public void testLazyLoadObject() throws NoSuchFieldException {
        user.setId(1);
        user = ((LazyUser) DAO.load(user));//load user with the id one attribute interest has a proxy object of Interest now
        assertNull(user.interests.name);//null because not loaded yet
        user.interests.toString();//call any method to load
        assertEquals("Tennis", user.interests.name);
        Log.write("Loaded " + user.interests.name + " and this interest has a priority of " + user.interests.priority, LogLevel.DEBUG);
    }

}
