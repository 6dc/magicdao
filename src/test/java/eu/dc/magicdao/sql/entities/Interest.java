package eu.dc.magicdao.sql.entities;

import eu.dc.magicdao.annotations.Column;
import eu.dc.magicdao.annotations.Table;

/**
 * Created by dc on 02.03.2016.
 */

@Table(tablename = "i_interests")
public class Interest {
    @Column(fieldname = "i_name", primarykey = true)
    public String name;

    @Column(fieldname = "i_prio")
    public int priority;
}
