package eu.dc.magicdao.sql.entities;

import eu.dc.magicdao.annotations.Column;
import eu.dc.magicdao.annotations.Table;

import java.awt.*;

/**
 * Created by dc on 02.03.2016.
 */
@Table(tablename = "u_s_settings")
public class Setting {
    @Column(fieldname = "color")
    Color color;

}
