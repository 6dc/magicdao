package eu.dc.magicdao.sql.entities;

import eu.dc.magicdao.annotations.Column;
import eu.dc.magicdao.annotations.Table;

/**
 * Created by me on 15.06.2016.
 */
@Table(tablename = "f_flaschen")
public class Flasche {

    @Column(fieldname = "f_id", primarykey = true)
    int id;

    @Column(fieldname = "f_name")
    String name;
    @Column(fieldname = "f_u_id")
    int u_id;
}
