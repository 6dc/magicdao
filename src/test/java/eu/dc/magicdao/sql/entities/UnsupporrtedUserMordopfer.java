package eu.dc.magicdao.sql.entities;

import eu.dc.magicdao.interfaces.DAOOperator;
import eu.dc.magicdao.util.Log;
import eu.dc.magicdao.util.enums.LogLevel;

import java.util.List;

/**
 * Created by me on 15.08.2016.
 */
public class UnsupporrtedUserMordopfer implements DAOOperator {
    @Override
    public Object insert(Object target) {
        Log.write("es wurde inserted", LogLevel.EMERGENCY);
        return null;
    }

    @Override
    public Object load(Object target) {
        Log.write("es wurde load", LogLevel.EMERGENCY);
        return null;
    }

    @Override
    public Object update(Object target) {
        Log.write("es wurde gewandert", LogLevel.EMERGENCY);
        return null;
    }

    @Override
    public Object delete(Object target) {
        Log.write("es wurde vernichtet", LogLevel.EMERGENCY);
        return null;
    }

    @Override
    public List<Object> findby(Class target, String where) {
        Log.write("ein detectiv hat es gefoundet", LogLevel.EMERGENCY);
        return null;
    }

    @Override
    public List<Object> findAll(Class c) {
        Log.write("der fall sei gelobt wurden sein werden worden gewesen, gg", LogLevel.EMERGENCY);
        return null;
    }
}
