package eu.dc.magicdao;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static java.sql.DriverManager.getConnection;

public class HSQLTest {


    private Connection connection;

    @Before
    public void setup() throws SQLException {
        connection = getConnection("jdbc:hsqldb:mem:quiz", "SA", "");
        connection.createStatement().execute("DROP SCHEMA PUBLIC CASCADE");
        connection.createStatement().execute("CREATE TABLE u_users( u_id INT , u_username VARCHAR(250), u_password VARCHAR(250), primary key(u_id))");
        connection.createStatement().execute("INSERT INTO u_users( u_id  , u_username, u_password) VALUES (2, 'test', 'test')");

    }

    @After
    public void teardown() throws SQLException {
        if (connection != null && !connection.isClosed()) {
            connection.close();
        }
    }

    @Test
    public void spikeReadingDataTroughJdbc() throws SQLException {
        // create JDBC connection
        PreparedStatement preparedStatement =
                connection.prepareStatement("SELECT * FROM u_users");
//        preparedStatement.setString(1, 1);
        try (ResultSet resultSet = preparedStatement.executeQuery()) {
            while (resultSet.next()) {
                String text = resultSet.getString(2);
                int correct = resultSet.getInt(1);

            }
        }
        preparedStatement.close();
    }


}
