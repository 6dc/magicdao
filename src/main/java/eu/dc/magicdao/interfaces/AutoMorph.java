package eu.dc.magicdao.interfaces;

public interface AutoMorph<T> {
    T morph(String s);

    String deMorph(T t);
}