package eu.dc.magicdao.interfaces;

import java.util.List;

/**
 * Created by me on 10.04.2016.
 */
public interface DAOOperator {

    Object insert(Object target);

    Object load(Object target);

    Object update(Object target);

    Object delete(Object target);

    List<Object> findby(Class target, String where);

    List<Object> findAll(Class c);


}
