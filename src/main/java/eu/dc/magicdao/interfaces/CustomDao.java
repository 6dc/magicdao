package eu.dc.magicdao.interfaces;

import java.util.List;

/**
 * Created by me on 15.08.2016.
 */
public interface CustomDao {

    Object insert(Object target);

    Object load(Object target);

    Object update(Object target);

    List<Object> findby(Class target, String where);

    List<Object> findAll(Class c);
}
