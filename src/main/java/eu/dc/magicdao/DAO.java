package eu.dc.magicdao;

import eu.dc.magicdao.interfaces.DAOOperator;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by dc on 07.12.2015.
 */
public class DAO {

    public static ConcurrentHashMap<Class, DAOEntity> registeredClasses = new ConcurrentHashMap<>();
    public static DAOOperator operator;

    public static void init(DAOOperator op) {
        operator = op;
    }

    public static DAOOperator getOperator() {
        return operator;
    }

    public static void setOperator(DAOOperator operator) {
        DAO.operator = operator;
    }

    public static Object insert(Object target) {
        return operator.insert(target);
    }

    public static Object load(Object target) {
        return operator.load(target);
    }

    public static Object delete(Object target) {
        return operator.delete(target);
    }

    public static Object update(Object target) {
        return operator.update(target);
    }

    public static List<Object> findby(Class target, String fieldie) {
        return operator.findby(target, fieldie);
    }

    public static List<Object> findAll(Class c) {
        return operator.findAll(c);
    }

    public static boolean prePareForAction(Class c) {
        if (DAO.registeredClasses.containsKey(c))
            return true;
        else {
            boolean suceeded = DomainClassHandler.registerMagicDaoClass(c);
            EventHandler.fireEventNewDAOEntityEvent(DAO.registeredClasses.get(c));
            return suceeded;
        }
    }
}