package eu.dc.magicdao.operator.defaults.SQL;

import eu.dc.magicdao.exception.MagicDaoException;
import eu.dc.magicdao.util.Log;
import eu.dc.magicdao.util.enums.LogLevel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by dc on 11.04.2016.
 */
public class SQLOperations {
    public Connection connection;

    public SQLOperations(Connection connection) {
        this.connection = connection;
    }

    public boolean isConnectionReady() {
        try {
            if (connection == null || connection.isClosed())
                throw new MagicDaoException("Init connection first!");
        } catch (SQLException e) {
            throw new MagicDaoException("Coudln't check if the connection is closed!");
        }
        return true;
    }

    public ResultSet execute(String query) {
        try {
            PreparedStatement statement = connection.prepareStatement(query);
            if (statement.execute()) {
                ResultSet rs = statement.getResultSet();
                return rs;
            }
        } catch (SQLException e) {
            Log.write(("Could not execute: " + query), LogLevel.DEBUG);
            throw new MagicDaoException(e, "Couldn't execute Query");
        }
        return null; //Querys which have no result, return null
    }
}
