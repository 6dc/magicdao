package eu.dc.magicdao.operator.defaults.SQL;

import eu.dc.magicdao.DAOEntity;
import eu.dc.magicdao.EventHandler;
import eu.dc.magicdao.Morphium;
import eu.dc.magicdao.interfaces.DAOOperator;
import eu.dc.magicdao.operator.defaults.SQL.murphyslaw.ColorMurph;
import eu.dc.magicdao.operator.defaults.SQL.murphyslaw.DateMurph;
import eu.dc.magicdao.operator.defaults.SQL.operations.DeleteOperation;
import eu.dc.magicdao.operator.defaults.SQL.operations.InsertOperation;
import eu.dc.magicdao.operator.defaults.SQL.operations.LoadOperation;
import eu.dc.magicdao.operator.defaults.SQL.operations.UpdateOperation;
import lombok.AccessLevel;
import lombok.Getter;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.awt.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by me on 10.04.2016.
 */
public class SQLDAO implements DAOOperator {
    @Getter(AccessLevel.PUBLIC)
    private static ConcurrentHashMap<Class, Class> proxysroot = new ConcurrentHashMap<>();
    @Getter(AccessLevel.PUBLIC)
    private static ArrayList<Object> loadedBuggy = new ArrayList<>();

    @Getter(AccessLevel.PUBLIC)
    Connection connection;

    @Getter(AccessLevel.PUBLIC)
    HashMap<DAOEntity, SQLPreparedStatements> preparedStatementsCache = new HashMap<>();

    @Getter(AccessLevel.PUBLIC)
    SQLOperations execute;

    public SQLDAO(Connection connection) {
        this.connection = connection;
        this.execute = new SQLOperations(connection);
        initEvent();
        initDefaultMurphs();
    }

    void initDefaultMurphs() {
        Morphium.insertMorph(Date.class, new DateMurph());
        Morphium.insertMorph(Color.class, new ColorMurph());
    }

    void initEvent() {
        EventHandler.registerDaoEntityListener(new PreparedStatementListener(this));
    }

    @Override
    public Object insert(Object target) {
        return new InsertOperation(this, target).insert();
    }

    @Override
    public Object load(Object target) {
        return new LoadOperation(this, target).load();
    }

    @Override
    public Object update(Object target) {
        return new UpdateOperation(this, target).update();
    }

    @Override
    public Object delete(Object target) {
        return new DeleteOperation(this, target).delete();
    }

    @Override
    public List<Object> findby(Class target, String fieldie) {//TODO
        throw new NotImplementedException();
    }

    @Override
    public List<Object> findAll(Class c) {
        return findby(c, "NULL IS NULL");
    } //Always true statement

    public ResultSet query(String statement, String ... param) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(statement);
        int i = 0;
        for(String p : param) {
            preparedStatement.setString(i, p);
            i++;
        }
        return preparedStatement.executeQuery();
    }

}
