package eu.dc.magicdao.operator.defaults.SQL.operations;

import eu.dc.magicdao.DAO;
import eu.dc.magicdao.DAOEntity;
import eu.dc.magicdao.Morphium;
import eu.dc.magicdao.annotations.Join;
import eu.dc.magicdao.exception.MagicDaoException;
import eu.dc.magicdao.operator.defaults.SQL.SQLDAO;
import eu.dc.magicdao.util.Log;
import eu.dc.magicdao.util.enums.LogLevel;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by me on 15.06.2016.
 */
public class InsertOperation {

    SQLDAO sqldao;
    Object target;

    public InsertOperation(SQLDAO sqldao, Object target) {
        if (!sqldao.getExecute().isConnectionReady())
            throw new MagicDaoException("Connection wasn't open");
        if (!DAO.prePareForAction(target.getClass()))
            throw new MagicDaoException("Class couldn't be prepared");
        this.sqldao = sqldao;
        this.target = target;
    }

    public Object insert() {
        DAOEntity dao = DAO.registeredClasses.get(target.getClass());

        int i = 1;
        for (Field f : dao.fields) {
            try {
                if (Morphium.registeredMorphs.containsKey(f.getType())) {
                    sqldao.getPreparedStatementsCache().get(dao).insert.setString(i, Morphium.registeredMorphs.get(f.getType()).deMorph(f.get(target)));
                } else {
                    if (f.isAnnotationPresent(Join.class)) {
                        //primary keys
                    } else {
                        sqldao.getPreparedStatementsCache().get(dao).insert.setString(i, f.get(target).toString());
                    }
                }
                i++;
            } catch (IllegalAccessException | SQLException e) {
                throw new MagicDaoException(e, "Couldn't set prepared statement parameters");
            }
        }
        try {
            sqldao.getPreparedStatementsCache().get(dao).insert.execute();
            ResultSet rs = sqldao.getPreparedStatementsCache().get(dao).insert.getGeneratedKeys();
            if (rs.next()) {
                Log.write("GeneratedPKS" + rs.getString(1), LogLevel.DEBUG);
            }
        } catch (SQLException e) {
            throw new MagicDaoException(e, "Couldn't execute prepared statement");
        }
        for(Field f : dao.custom.keySet())
            dao.custom.get(f).insert(target);

        return true;
    }
}
