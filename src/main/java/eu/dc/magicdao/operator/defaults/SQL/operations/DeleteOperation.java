package eu.dc.magicdao.operator.defaults.SQL.operations;

import eu.dc.magicdao.DAO;
import eu.dc.magicdao.DAOEntity;
import eu.dc.magicdao.Morphium;
import eu.dc.magicdao.annotations.Join;
import eu.dc.magicdao.exception.MagicDaoException;
import eu.dc.magicdao.operator.defaults.SQL.SQLDAO;
import eu.dc.magicdao.util.Log;
import eu.dc.magicdao.util.enums.LogLevel;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by me on 15.06.2016.
 */
public class DeleteOperation {

    SQLDAO sqldao;
    Object target;

    public DeleteOperation(SQLDAO sqldao, Object target) {
        if (!sqldao.getExecute().isConnectionReady())
            throw new MagicDaoException("Connection wasn't open");
        if (!DAO.prePareForAction(target.getClass()))
            throw new MagicDaoException("Class couldn't be prepared");
        this.sqldao = sqldao;
        this.target = target;
    }

    public Object delete() {
        DAOEntity dao = DAO.registeredClasses.get(target.getClass());

        int i = 1;

        for (Field f : dao.primary_keys) {
            try {
                f.setAccessible(true);
                sqldao.getPreparedStatementsCache().get(dao).delete.setString(i, f.get(target).toString());
                i++;
            } catch (IllegalAccessException | SQLException e) {
                throw new MagicDaoException(e, "Couldn't laod primary key field");
            }
        }

        for(Field f : dao.custom.keySet())
            dao.custom.get(f).delete(target);

        try {
            sqldao.getPreparedStatementsCache().get(dao).delete.execute();
        } catch (SQLException e) {
            throw new MagicDaoException(e, "Delete couldn't be performed");
        }


        return true;
    }
}
