package eu.dc.magicdao.operator.defaults.SQL;

import eu.dc.magicdao.DAOEntity;
import eu.dc.magicdao.events.DAOEntityListener;

/**
 * Created by me on 15.06.2016.
 */
public class PreparedStatementListener implements DAOEntityListener {

    SQLDAO sqldao;

    public PreparedStatementListener(SQLDAO sqldao) {
        this.sqldao = sqldao;
    }

    @Override
    public void newDAOEntityCreatedEvent(DAOEntity entity) {
        SQLPreparedStatements preparedStatements = new SQLPreparedStatements(entity, sqldao.execute);
        preparedStatements.prepareStatements();
        sqldao.preparedStatementsCache.put(entity, preparedStatements);
    }
}
