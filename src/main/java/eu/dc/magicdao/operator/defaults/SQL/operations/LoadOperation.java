package eu.dc.magicdao.operator.defaults.SQL.operations;

import eu.dc.magicdao.DAO;
import eu.dc.magicdao.DAOEntity;
import eu.dc.magicdao.Morphium;
import eu.dc.magicdao.annotations.Join;
import eu.dc.magicdao.exception.MagicDaoException;
import eu.dc.magicdao.operator.defaults.SQL.SQLDAO;
import eu.dc.magicdao.util.Log;
import eu.dc.magicdao.util.enums.LogLevel;
import eu.dc.magicdao.util.enums.Relation;
import javassist.util.proxy.MethodHandler;
import javassist.util.proxy.ProxyFactory;

import java.lang.reflect.Field;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by me on 15.06.2016.
 */
public class LoadOperation {

    SQLDAO sqldao;
    Object target;

    public LoadOperation(SQLDAO sqldao, Object target) {
        if (!sqldao.getExecute().isConnectionReady())
            throw new MagicDaoException("Connection wasn't open");
        if (!DAO.prePareForAction(target.getClass()))
            throw new MagicDaoException("Class couldn't be prepared");
        this.sqldao = sqldao;
        this.target = target;
    }

    public Object load() {
        DAOEntity dao = DAO.registeredClasses.get(target.getClass());
        ArrayList<Field> primary_key = dao.primary_keys;
        try {
            int i = 1;
            for (Field f : primary_key) {
                try {
                    f.setAccessible(true);
                    sqldao.getPreparedStatementsCache().get(dao).load.setString(i, f.get(target).toString());
                    i++;
                } catch (IllegalAccessException | SQLException e) {
                    throw new MagicDaoException(e, "Couldn't laod primary key field");
                }
            }

            Log.write(sqldao.getPreparedStatementsCache().get(dao).load.toString(), LogLevel.DEBUG);
            ResultSet resultSet = sqldao.getPreparedStatementsCache().get(dao).load.executeQuery();
            Log.write(resultSet.toString(), LogLevel.DEBUG);
            resultSet.next();


            Object obj = target;
            for (Field field : dao.fields) {
                field.setAccessible(true);
                try {
                    field.set(obj, resultSet.getObject(dao.getFieldName(field)));
                } catch (Exception e) {
                    if (Morphium.registeredMorphs.containsKey(field.getType()))
                        field.set(obj, Morphium.registeredMorphs.get(field.getType()).morph(
                                (String) resultSet.getObject(dao.getFieldName(field))));
                    else {
                        if (field.isAnnotationPresent(Join.class)) {
                            ProxyFactory factory = new ProxyFactory();
                            factory.setSuperclass(field.getType());

                            MethodHandler handler = (self, thisMethod, proceed, args) -> {
                                if (!SQLDAO.getLoadedBuggy().contains(self.getClass())) {
                                    SQLDAO.getProxysroot().put(self.getClass(), field.getType());
                                    //Log.write("FIRST Lazy load: " + self.getClass().getName() + "/" + SQLDAO.getLoadedBuggy().get(self.getClass()).getName(), LogLevel.DEBUG);
                                    SQLDAO.getLoadedBuggy().add(self.getClass());
                                    if (field.getAnnotation(Join.class).relation() == Relation.OneToOne)
                                        self = loadJoin(field, target, self, resultSet.getObject(dao.getFieldName(field)));

                                }
                                return proceed.invoke(self, args);
                            };
                            Object dog = factory.create(new Class<?>[0], new Object[0], handler);
                            field.set(obj, dog);
                        } else {
                            Log.write("no morphs & no joins found", LogLevel.DEBUG);
                        }
                    }
                }
            }
            for(Field f : dao.custom.keySet())
                dao.custom.get(f).load(f);

            return obj;

        } catch (Exception e) {
            throw new MagicDaoException(e, "Couldn't laod join or anything else");
        }
    }

    public Object loadJoin(Field rootField, Object rootObject, Object target, Object value) {
        if (!sqldao.getExecute().isConnectionReady())
            return false;
        if (!DAO.prePareForAction(SQLDAO.getProxysroot().get(target.getClass())))
            return false;

        DAOEntity rootEntity = DAO.registeredClasses.get(rootObject.getClass());
        DAOEntity targetEntity = DAO.registeredClasses.get(SQLDAO.getProxysroot().get(target.getClass()));

        String rootTableName = rootEntity.table_name;
        String rootColumnName = rootEntity.getFieldName(rootField);

        String destTableName = targetEntity.table_name;
        String destColumnName = targetEntity.getFieldName(targetEntity.primary_keys.get(0));

        List<Object> list = new ArrayList<>();
        try {

            String query = "SELECT " + destTableName + ".*" + " FROM " + rootTableName + " " +
                    "INNER JOIN " + destTableName + " " +
                    "ON " + rootTableName + "." + rootColumnName + "=" + destTableName + "." + destColumnName + " " +
                    "WHERE " + destTableName + "." + destColumnName + "='" + value + "';";//value= rootField.getInt(rootObject)
            Log.write(query, LogLevel.DEBUG);
            PreparedStatement loadJoin = sqldao.getConnection().prepareStatement(query);
            ResultSet resultSet = loadJoin.executeQuery();

            Log.write(resultSet.toString(), LogLevel.DEBUG);

            while (resultSet.next()) {

                Object obj = target;
                for (Field field : targetEntity.fields) {
                    field.setAccessible(true);
                    try {
                        field.set(obj, resultSet.getObject(targetEntity.getFieldName(field)));
                    } catch (Exception e) {
                        if (Morphium.registeredMorphs.containsKey(field.getType()))
                            field.set(obj, Morphium.registeredMorphs.get(field.getType()).morph(
                                    (String) resultSet.getObject(targetEntity.getFieldName(field))));
                        else {
                            if (field.isAnnotationPresent(Join.class)) {
                                ProxyFactory factory = new ProxyFactory();
                                if (field.getAnnotation(Join.class).relation() == Relation.OneToOne)
                                    factory.setSuperclass(field.getType());
                                else
                                    factory.setSuperclass((Class) field.getGenericType());

                                final Object finalTarget = target;
                                MethodHandler handler = (self, thisMethod, proceed, args) -> {
                                    Log.write("Name: " + finalTarget.getClass().getName(), LogLevel.DEBUG);
                                    if (!SQLDAO.getLoadedBuggy().contains(self)) {
                                        SQLDAO.getLoadedBuggy().add(self);
                                        SQLDAO.getProxysroot().put(self.getClass(), field.getType());
                                        // self = DAO.loadJoin(field, target, self);
                                    }
                                    return proceed.invoke(self, args);
                                };
                                Object dog = factory.create(new Class<?>[0], new Object[0], handler);
                                field.set(obj, dog);
                            } else {
                                Log.write("no morphs & no joins found", LogLevel.DEBUG);
                            }
                        }
                    }
                }
                list.add(obj);
            }
        } catch (Exception e) {
            throw new MagicDaoException(e, "Couldn't laod join");
        }

        if (rootField.getAnnotation(Join.class).relation() == Relation.OneToOne)
            target = list.get(0);


        return target;

    }

}
