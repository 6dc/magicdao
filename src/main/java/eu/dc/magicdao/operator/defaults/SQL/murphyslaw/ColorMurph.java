package eu.dc.magicdao.operator.defaults.SQL.murphyslaw;

import eu.dc.magicdao.interfaces.AutoMorph;

import java.awt.*;

/**
 * Created by dc on 23.02.2016.
 */
public class ColorMurph implements AutoMorph<Color> {
    @Override
    public Color morph(String s) {

        return new Color(Integer.parseInt(s));
    }

    @Override
    public String deMorph(Color color) {
        return String.valueOf(color.getRGB());
    }
}
