package eu.dc.magicdao.operator.defaults.mongodb;

import com.mongodb.BasicDBObject;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import eu.dc.magicdao.DAO;
import eu.dc.magicdao.DAOEntity;
import eu.dc.magicdao.exception.MagicDaoException;
import eu.dc.magicdao.interfaces.DAOOperator;
import org.bson.Document;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

/**
 * Created by dc on 22.04.2016.
 */
@Deprecated
public class MongoDB implements DAOOperator {
    private MongoDatabase connection;

    public MongoDB(MongoDatabase connection) {
        this.connection = connection;
    }

    public Object insert(Object target) {
        if (!DAO.prePareForAction(target.getClass()))
            return false;
        DAOEntity entity = DAO.registeredClasses.get(target.getClass());
        Document sb = new Document();
        try {
            for (int i = 0; i < entity.fields.size(); i++) {
                Field f = entity.fields.get(i);
                f.setAccessible(true);
                if (f.get(target) != null)
                    sb.append(entity.getFieldName(f), f.get(target));
            }
        } catch (Exception e) {
            throw new MagicDaoException(e, "Couldn't insert user to database");
        }
        connection.getCollection(entity.table_name).insertOne(sb);
        return true;
    }

    public Object load(Object target) {
        DAOEntity entity = DAO.registeredClasses.get(target.getClass());
        Document sb = new Document();
        try {
            for (int i = 0; i < entity.fields.size(); i++) {
                Field f = entity.fields.get(i);
                f.setAccessible(true);
                if (entity.primary_keys.contains(f))
                    sb.append(entity.getFieldName(f), f.get(target));
            }
        } catch (Exception e) {
            throw new MagicDaoException(e, "Couldn't load object data from database");
        }
        Document d = connection.getCollection(entity.table_name).find(sb).first();
        try {
            for (int i = 0; i < entity.fields.size(); i++) {
                Field f = entity.fields.get(i);
                f.setAccessible(true);
                if (!entity.primary_keys.contains(f))
                    f.set(target, d.get(entity.getFieldName(f)));
            }
        } catch (Exception e) {
            throw new MagicDaoException(e, "Couldn't fill fields of java object with data from the database");
        }
        return target;
    }

    public String getPrimaryKeyValue(Document document) {
        return document.getString("_id");
    }

    public Object update(Object target) {
        if (!DAO.prePareForAction(target.getClass()))
            return false;
        DAOEntity entity = DAO.registeredClasses.get(target.getClass());
        BasicDBObject sb = new BasicDBObject();
        BasicDBObject set = new BasicDBObject();
        BasicDBObject idDocument = new BasicDBObject();
        sb.append("$set", set);
        try {
            for (int i = 0; i < entity.fields.size(); i++) {
                Field f = entity.fields.get(i);
                f.setAccessible(true);
                if (entity.primary_keys.contains(f))
                    idDocument.append(entity.getFieldName(f), f.get(target));
                else
                    set.append(entity.getFieldName(f), f.get(target));
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        connection.getCollection(entity.table_name).updateOne(idDocument, sb);
        return true;
    }

    @Override
    public Object delete(Object target) {
        throw new NotImplementedException(); //lvl up m8
    }

    public List<Object> findby(Class target, String fieldie) {
        throw new MagicDaoException(new NotImplementedException(), "Findby is not implemented yet");
    }

    public List<Object> findAll(Class c) {
        DAO.prePareForAction(c);
        DAOEntity entity = DAO.registeredClasses.get(c);
        MongoCollection<Document> x = connection.getCollection(entity.table_name);
        FindIterable<Document> iter = x.find(new BasicDBObject());
        List<Object> re = new ArrayList<>();
        iter.forEach((Consumer<Document>) document -> {
            try {
                Object neew = c.newInstance();
                for (int i = 0; i < entity.fields.size(); i++) {
                    Field f = entity.fields.get(i);
                    f.setAccessible(true);
                    if (entity.primary_keys.contains(f))
                        f.set(neew, document.get(entity.getFieldName(f)).toString());
                    else
                        f.set(neew, document.get(entity.getFieldName(f)));
                }
                re.add(neew);
            } catch (Exception e) {
                throw new MagicDaoException(e, "Couldn't proceed findall");
            }
        });
        return re;
    }
}
