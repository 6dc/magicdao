package eu.dc.magicdao.operator.defaults.SQL.operations;

import eu.dc.magicdao.DAO;
import eu.dc.magicdao.DAOEntity;
import eu.dc.magicdao.Morphium;
import eu.dc.magicdao.annotations.Column;
import eu.dc.magicdao.exception.MagicDaoException;
import eu.dc.magicdao.operator.defaults.SQL.SQLDAO;

import java.lang.reflect.Field;
import java.sql.SQLException;

/**
 * Created by me on 15.06.2016.
 */
public class UpdateOperation {

    SQLDAO sqldao;
    Object target;

    public UpdateOperation(SQLDAO sqldao, Object target) {
        if (!sqldao.getExecute().isConnectionReady())
            throw new MagicDaoException("Connection wasn't open");
        if (!DAO.prePareForAction(target.getClass()))
            throw new MagicDaoException("Class couldn't be prepared");
        this.sqldao = sqldao;
        this.target = target;
    }

    public Object update() {
        DAOEntity dao = DAO.registeredClasses.get(target.getClass());
        try {
            int x = 1;
            for (int i = 0; i < dao.fields.size(); i++) {
                dao.fields.get(i).setAccessible(true);
                if (dao.fields.get(i).isAnnotationPresent(Column.class)) {
                    try {
                        if (Morphium.registeredMorphs.containsKey(dao.fields.get(i).getType())) {
                            sqldao.getPreparedStatementsCache().get(dao).update.setString(x, Morphium.registeredMorphs.get(dao.fields.get(i).getType()).deMorph(dao.fields.get(i).get(target)));
                        } else {
                            sqldao.getPreparedStatementsCache().get(dao).update.setString(x, dao.fields.get(i).get(target).toString());
                        }
                        x++;
                    } catch (SQLException | IllegalAccessException e) {
                        throw new MagicDaoException(e, "Coudlnt set parameters of prepared statement");
                    }
                }
            }

            for (int i = 0; i < dao.fields.size(); i++) {

                dao.fields.get(i).setAccessible(true);
                if (dao.fields.get(i).getAnnotation(Column.class).primarykey()) {
                    if (Morphium.registeredMorphs.containsKey(dao.fields.get(i).getType())) {
                        sqldao.getPreparedStatementsCache().get(dao).update.setString(x, Morphium.registeredMorphs.get(dao.fields.get(0).getType()).deMorph(dao.fields.get(i).get(target)));
                    } else
                        sqldao.getPreparedStatementsCache().get(dao).update.setString(x, dao.fields.get(i).get(target).toString());
                    x++;
                }

            }

            sqldao.getPreparedStatementsCache().get(dao).update.execute();

            for(Field f : dao.custom.keySet())
                dao.custom.get(f).update(target);
        } catch (Exception e) {
            throw new MagicDaoException(e, "Coudln't set prepared fields for the update query");
        }

        return true;
    }
}
