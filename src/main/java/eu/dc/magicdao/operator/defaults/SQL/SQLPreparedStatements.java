package eu.dc.magicdao.operator.defaults.SQL;

import com.mysql.jdbc.Statement;
import eu.dc.magicdao.DAOEntity;
import eu.dc.magicdao.annotations.Column;
import eu.dc.magicdao.exception.MagicDaoException;
import eu.dc.magicdao.util.Log;
import eu.dc.magicdao.util.enums.LogLevel;

import java.lang.reflect.Field;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by dc on 11.04.2016.
 */
public class SQLPreparedStatements {

    public PreparedStatement insert;
    public PreparedStatement load;
    public PreparedStatement update;
    public PreparedStatement delete;
    public PreparedStatement findby;//TODO
    public PreparedStatement findall;//TODO

    public SQLOperations executer;

    DAOEntity entity;

    public SQLPreparedStatements(DAOEntity entity, SQLOperations executer) {
        this.entity = entity;
        this.executer = executer;
    }

    public void prepareStatements() {
        prepareInsertStatement();
        prepareUpdateStatement();
        prepareLoadStatement();
        prepareDeleteStatement();
    }

    private void prepareInsertStatement() {
        try {
            StringBuilder builder = new StringBuilder();
            builder.append("INSERT INTO " + entity.table_name + " (");
            for (Field f : entity.fields) {
                builder.append(entity.getFieldName(f) + ",");
            }
            builder.deleteCharAt(builder.toString().length() - 1);
            builder.append(") VALUES (");
            for (Field f : entity.fields) {
                f.setAccessible(true);
                builder.append(("?") + ",");
            }
            builder.deleteCharAt(builder.toString().length() - 1);
            builder.append(")");
            Log.write(builder.toString(), LogLevel.DEBUG);
            insert = executer.connection.prepareStatement(builder.toString(), Statement.RETURN_GENERATED_KEYS);
        } catch (SQLException e) {
            new MagicDaoException(e, "Coudln't create Prepared Insert Statement");
        }
    }

    private void prepareUpdateStatement() {
        try {
            StringBuilder builder = new StringBuilder();
            builder.append("UPDATE " + entity.table_name);
            builder.append(" SET ");
            for (Field f : entity.fields) {
                f.setAccessible(true);
                builder.append(entity.getFieldName(f) + "= ?,");
            }
            builder.deleteCharAt(builder.toString().length() - 1);
            boolean where = true;
            for (int i = 0; i < entity.fields.size(); i++) {
                entity.fields.get(i).setAccessible(true);
                if (entity.fields.get(i).getAnnotation(Column.class).primarykey()) {
                    builder.append((where ? " WHERE " : " AND ") +
                            entity.getFieldName(entity.fields.get(i)) + "= " + "?");
                    where = false;
                }
            }
            builder.append(";");
            Log.write(builder.toString(), LogLevel.DEBUG);

            update = executer.connection.prepareStatement(builder.toString());
        } catch (SQLException e) {
            new MagicDaoException(e, "Coudln't create Prepared Update Statement");
        }
    }

    public void prepareLoadStatement() {
        try {
            ArrayList<Field> primary_key = entity.primary_keys;

            StringBuilder builder = new StringBuilder();
            builder.append("SELECT * FROM ").append(entity.table_name);
            builder.append(" WHERE ");
            for (Field f : primary_key) {
                builder.append(f.getAnnotation(Column.class).fieldname());
                builder.append("=");
                f.setAccessible(true);
                builder.append("?");
                builder.append(" AND ");
            }

            String text = builder.substring(0, builder.length() - 4) + ";";
            Log.write(text.toString(), LogLevel.DEBUG);
            load = executer.connection.prepareStatement(text);
        } catch (SQLException e) {
            new MagicDaoException(e, "Coudln't create Prepared Load Statement");
        }
    }

    public void prepareDeleteStatement() {
        try {
            ArrayList<Field> primary_key = entity.primary_keys;

            StringBuilder builder = new StringBuilder();
            builder.append("DELETE FROM ").append(entity.table_name);
            builder.append(" WHERE ");
            for (Field f : primary_key) {
                builder.append(f.getAnnotation(Column.class).fieldname());
                builder.append("=");
                f.setAccessible(true);
                builder.append("?");
                builder.append(" AND ");
            }

            String text = builder.substring(0, builder.length() - 4) + ";";
            Log.write(text.toString(), LogLevel.DEBUG);
            delete = executer.connection.prepareStatement(text);
        } catch (SQLException e) {
            new MagicDaoException(e, "Coudln't create Prepared Delete Statement");
        }
    }

}
