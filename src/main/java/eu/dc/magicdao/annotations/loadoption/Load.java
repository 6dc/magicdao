package eu.dc.magicdao.annotations.loadoption;

import eu.dc.magicdao.util.enums.Loader;

/**
 * Created by dc on 02.03.2016.
 */
public @interface Load {
    Loader loader() default Loader.lazy;
}
