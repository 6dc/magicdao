package eu.dc.magicdao;

import eu.dc.magicdao.annotations.Table;
import eu.dc.magicdao.util.JarUtil;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;

/**
 * Created by me on 10.04.2016.
 */
public class DomainClassHandler {
    //Doesn't work with JUNIT you can use it when you run the compiled jar file
    @Deprecated
    public static void registerAllClasses() throws UnsupportedEncodingException {
        String path = DAO.class.getProtectionDomain().getCodeSource().getLocation().getPath();
        String decodedPath = URLDecoder.decode(path, "UTF-8");
        List<Class<?>> classes = JarUtil.getClassesFromJar(new File(decodedPath));
        for (Class c : classes) {
            if (c.isAnnotationPresent(Table.class)) {
                registerMagicDaoClass(c);
            }
        }
    }

    public static boolean registerMagicDaoClass(Class c) {
        DAOEntity entity = DAOEntity.makeDaoEntity(c);
        DAO.registeredClasses.put(c, entity);
        return entity != null;
    }
}
