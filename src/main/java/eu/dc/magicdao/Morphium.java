package eu.dc.magicdao;

import eu.dc.magicdao.interfaces.AutoMorph;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by dc on 22.02.2016.
 */
public class Morphium {
    public static ConcurrentHashMap<Class, AutoMorph> registeredMorphs = new ConcurrentHashMap<>();

    public static void insertMorph(Class c, AutoMorph morph) {
        registeredMorphs.put(c, morph);
    }

    public static void removeMorph(Class c) {
        registeredMorphs.remove(c);
    }

}
