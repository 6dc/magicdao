package eu.dc.magicdao.util.enums;

/**
 * Created by dc on 12.03.2016.
 */
public enum Relation {
    OneToOne, OneToMany, Relation;
}
