package eu.dc.magicdao.util;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * Created by me on 10.04.2016.
 */
public class JarUtil {
    @SuppressWarnings("resource")
    public static List<Class<?>> getClassesFromJar(File file) {
        Class<?> classSearched = null;
        String packageName = "";
        List<Class<?>> classes = new ArrayList<>();
        String dirSearched = packageName.replace(".", "/");
        ZipFile zipFile = null;
        try {
            zipFile = new ZipFile(file);
        } catch (Exception ex) {
            return classes;
        }
        for (Enumeration<? extends ZipEntry> zipEntries = zipFile.entries(); zipEntries.hasMoreElements(); ) {
            String entryName = zipEntries.nextElement().getName();
            if (!entryName.startsWith(dirSearched) ||
                    !entryName.toLowerCase().endsWith(".class"))
                continue;
            entryName = entryName.substring(0, entryName.length() - ".class".length());
            entryName = entryName.replace("/", ".");
            try {
                Class<?> clazz = Class.forName(entryName);
                if (classSearched == null || classSearched.isAssignableFrom(clazz))
                    classes.add(clazz);
            } catch (Throwable ex) {
            }
        }
        return Collections.unmodifiableList(classes);
    }
}
