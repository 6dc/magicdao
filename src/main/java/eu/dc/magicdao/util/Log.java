package eu.dc.magicdao.util;

import eu.dc.magicdao.util.enums.LogLevel;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;

/**
 * Created by dc on 15.01.2016.
 */
public class Log {
    private static LogLevel level = LogLevel.EMERGENCY;
    private static OutputStream out = System.out;

    public static void setLogLevel(LogLevel l) {
        level = l;
    }

    public static boolean write(String message, LogLevel lvl) {
        if (lvl.getLvl() <= level.getLvl()) {
            try {
                out.write((message + "\n").getBytes(Charset.forName("UTF-8")));
                return true;
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return false;
    }
}
