package eu.dc.magicdao;

import eu.dc.magicdao.events.DAOEntityListener;

import java.util.ArrayList;

/**
 * Created by dc on 11.04.2016.
 */
public class EventHandler {
    static ArrayList<DAOEntityListener> events = new ArrayList<>();

    public static void registerDaoEntityListener(DAOEntityListener listener) {
        events.add(listener);
    }

    public static void unregisterDaoEntityListener(DAOEntityListener listener) {
        events.remove(listener);
    }

    protected static void fireEventNewDAOEntityEvent(DAOEntity entity) {
        for (DAOEntityListener listener : events) {
            listener.newDAOEntityCreatedEvent(entity);
        }
    }
}
