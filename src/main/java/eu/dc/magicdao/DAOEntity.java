package eu.dc.magicdao;


import eu.dc.magicdao.annotations.Column;
import eu.dc.magicdao.annotations.Custom;
import eu.dc.magicdao.annotations.Table;
import eu.dc.magicdao.exception.MagicDaoException;
import eu.dc.magicdao.interfaces.DAOOperator;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;

public class DAOEntity {
    public String table_name;
    public ArrayList<Field> fields;
    public ArrayList<Field> primary_keys = new ArrayList<>();
    public HashMap<Field, DAOOperator> custom = new HashMap<>();


    private DAOEntity(String table_name, ArrayList<Field> fields, Class c) {
        this.table_name = table_name;
        this.fields = fields;
        loadPrimaryKeys();
        loadCustom(c);
    }

    public static DAOEntity makeDaoEntity(Class c) {
        String fieldname = ((Table) c.getAnnotation(Table.class)).tablename();
        ArrayList<Field> wugu = new ArrayList<>();
        for (Field f : c.getDeclaredFields()) {
            if (f.isAnnotationPresent(Column.class)) {
                wugu.add(f);
            }
        }
        return new DAOEntity(fieldname, wugu, c);
    }

    private void loadPrimaryKeys() {
        for (Field field : fields) {
            if (isPrimarykey(field)) {
                primary_keys.add(field);
            }
        }
    }

    private void loadCustom(Class c) {
        for (Field field : c.getDeclaredFields()) {
            if (field.isAnnotationPresent(Custom.class)) {
                Class aClass = field.getAnnotation(Custom.class).value();
                try {
                    DAOOperator operator = (DAOOperator) aClass.newInstance();
                    custom.put(field, operator);
                } catch (InstantiationException | IllegalAccessException e) {
                    throw new MagicDaoException("Value should be a eu.dc.magicdao.interfaces.DAOOperator class");
                }
            }
        }
    }

    public String getFieldName(Field field) {
        if (field.isAnnotationPresent(Column.class))
            return field.getAnnotation(Column.class).fieldname();
        throw new MagicDaoException("This field has not the Fill annotation!");
    }

    private boolean isPrimarykey(Field field) {
        return field.getAnnotation(Column.class).primarykey();
    }
}