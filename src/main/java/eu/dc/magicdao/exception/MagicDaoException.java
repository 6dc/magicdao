package eu.dc.magicdao.exception;

/**
 * Created by me on 08.06.2016.
 */
public class MagicDaoException extends RuntimeException {

    public MagicDaoException(Exception e, String text) {
        super(text, e);
    }

    public MagicDaoException(String text) {
        super(text);
    }
}
