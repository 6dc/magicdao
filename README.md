
# MagicDAO #
only pregenerated prepared statements are used for better performance and security


# Note #
You can currently use this for SQL and MongoDB, but you can always use it for different databases too, by implementing a DAO Operator. (DAO.init(new OPERATOR()))  

MongoDB tests are ignored, due to the fact that you need a local Mongo Database instance running on your local machiene to build.  
; )  

![ss+2016-06-15+at+07.54.29.png](https://bitbucket.org/repo/zd5exj/images/2186865591-ss+2016-06-15+at+07.54.29.png)



# Tutorial #
## Setting it up for SQL##
Initalize the connection in the beginning 
```
#!java

DAO.init(new SQLDAO(connection))
```

Just put before the class that stands for a table
```
#!java

@Table(name="t_tablename")
```

And for columns use
```
#!java

@Column(name="u_username")
```
 that are stored in the database
transient fields can be just ignored

PrimaryKey fields have to be highlighted with

```
#!java

@Column(name="u_id",primarykey=true)
```


An example from our unit test entities:

```
#!java

import eu.dc.magicdao.annotations.Column;
import eu.dc.magicdao.annotations.Table;

@Table(tablename = "u_users")
public class User {
    @Column(fieldname = "u_id", primarykey = true)
    public int id;
    @Column(fieldname = "u_username")
    public String username;
    @Column(fieldname = "u_password")
    public String password;

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
```

Relations:

```
#!java

@Join(relation = )
```

Possible values: OneToOne, OneToMany


```
#!java

@Load
```

default value is lazyloading -> if its used the first time it will get loaded

A proxy class is given to the field and if any method of the proxy class is called it will load the related object.


Example:
```
#!java
//field in user
@Column(fieldname = "u_i_interests")
@Load @Join(relation = Relation.OneToOne) public Interest interests;


//Interest
@Table(tablename = "i_interests")
public class Interest {
    @Column(fieldname = "i_name", primarykey = true)
    public String name;

    @Column(fieldname = "i_prio")
    public int priority;
}
```

If you now call any method of the interest it will load.


## Use it ##
Then you can DAO.load(any_object), DAO.save(any_object) etc anywhere



!Findby has no updated unit test so it's marked as deprecated
